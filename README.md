# MBI_MLMB_project

#### Project: Hyperparameter optimization of a methylation autoencoder

MedBioInfo Machine Learning in Medical Bioinformatics Project (June 2021).

Author: David Martínez.

#### Requirements:
The workflow has been tested with the following software and packages/modules.
- Python 3.8.5
  - TensorFlow 2.4.0
  - Keras 2.4.3
  - numPy 1.19.5
  - pandas 1.2.4
  - matplotlib 3.4.2
  - scikit-learn 0.24.2

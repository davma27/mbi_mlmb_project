#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
"""
Created on Mon Jun 21 11:26:13 2021

@author: davma27
"""

# Autoencoder for reconstruction of methylation data
# Project for Machine Learning in Medical Bioinformatics (MedBioInfo 2021)


# %% Import libraries
import random
from time import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from keras import optimizers
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint, TensorBoard
from keras.layers import Activation, Input, Dense, Dropout
from keras.models import Sequential, load_model
from sklearn.metrics import r2_score, mean_absolute_error
from sklearn.model_selection import KFold

# %%

########## S1. Multi-GPU Training ##########

# %% S1.1
# Check and start multi-GPU setup
tf.config.list_physical_devices("GPU")

strategy = tf.distribute.MirroredStrategy()
print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Hide GPUs from CUDA visible devices
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

# Set main and input directories
main_dir = "/home/davma27/Documents/MedBioInfo/Machine_Learning_in_Medical_Bioinformatics/Project/"
input_dir = "/home/davma27/Documents/deep_learning_methylomics/Data/EWAS_integrated_raw_filled_test/"

# Load data from main file
ewas_df = pd.read_csv((input_dir + "raw_filled_ewas_integ_mat.csv"), sep=",", na_values="NA",
                      index_col=0).astype("float32")

# %% S1.2
# Set architecture, hyperparameters, optimizer, callbacks
model_name = "MLproj"
data_name = "KNNfilttest"
act_fun_hl = "relu"
act_fun_fl = "sigmoid"
loss_fun = "mean_squared_error"

comp_dim = 64
n_samples = len(ewas_df.columns)
n_features = len(ewas_df.index)
n_epochs = 500
batch_size = 128
n_hls = 2
drop_prob = 0

use_csv_logger = True
use_early_stop = True
es_patience = 10
use_model_checkpoints = True
use_tensorboard = False

lr = 1e-3
k_folds = 5

# %% S1.3
# Divide samples in training, validation, and test sets
trainset_tag = "mlproject1"
prop_trainval = 0.8 # Train + validation vs. test set

try:
    rand_sets = pd.read_csv((main_dir + "Models/" + model_name + "_" +
                             data_name + "_" + trainset_tag + ".csv"),
                            header=None).loc[:, 0].values.tolist()
    rand_sets = list(map(int, rand_sets))
except IOError:
    rand_sets = random.sample(range(n_samples), n_samples)
    np.savetxt((main_dir + "Models/" + model_name + "_" + data_name + "_" +
                trainset_tag + ".csv"), rand_sets, delimiter=",")

# Create and transpose sets
s_train = ewas_df.iloc[:, rand_sets[0:round(prop_trainval*n_samples)]]
s_train = np.transpose(s_train)
s_train = s_train.to_numpy()

s_test = ewas_df.iloc[:, rand_sets[round(prop_trainval*n_samples):n_samples]]
s_test = np.transpose(s_test)
s_test = s_test.to_numpy()

del ewas_df

# %% S1.4
# Set model callbacks
callbacks = []

if use_csv_logger:
    save_log = (main_dir + "Models/" + model_name + "_" + str(n_hls) +
                "HL_" + data_name + "_" + act_fun_hl + "_" + str(comp_dim) +
                "nod_lr_" + str(lr) + "_drop" + str(drop_prob) + ".csv")
    csv_logger = CSVLogger(save_log, append=True, separator="\t")
    callbacks.append(csv_logger)

if use_early_stop:
    early_stop = EarlyStopping(monitor="val_loss", mode="min", verbose=1,
                               patience=es_patience)
    callbacks.append(early_stop)

if use_model_checkpoints:
    best_checkpoint = ModelCheckpoint((main_dir + "Models/" + model_name +
                                       "_" + str(n_hls) + "HL_" + data_name +
                                       "_" + act_fun_hl + "_" + str(comp_dim) +
                                       "nod_lr_" + str(lr) + "_drop" +
                                       str(drop_prob) + ".h5"),
                                      monitor="val_loss",
                                      save_best_only=True, save_weights_only=False,
                                      save_freq="epoch", mode="min", verbose=1)
    callbacks.append(best_checkpoint)

if use_tensorboard:
    tensorboard = TensorBoard(log_dir=(main_dir + "Models/TB_logs/{}").format(time()))
    callbacks.append(tensorboard)

# %% S1.5
# Compile and train model
with strategy.scope():

    # Define the k-fold cross validator
    kfold = KFold(n_splits=k_folds, shuffle=True, random_state=777)
    fold = 1

    # Set architecture and compile model
    for set_fold in kfold.split(s_train):
        autoencoder = Sequential()
        autoencoder.add(Input(shape=(n_features, )))
        autoencoder.add(Dense(comp_dim))
        autoencoder.add(Activation(act_fun_hl))
        autoencoder.add(Dropout(drop_prob))
        autoencoder.add(Dense(comp_dim))
        autoencoder.add(Activation(act_fun_hl))
        autoencoder.add(Dropout(drop_prob))
        autoencoder.add(Dense(n_features))
        autoencoder.add(Activation(act_fun_fl))
        adam_opt = optimizers.Adam(lr=lr, beta_1=0.9, beta_2=0.999,
                                   epsilon=1e-8, decay=1e-6)
        autoencoder.compile(optimizer=adam_opt, loss=loss_fun)

        print(f"Training for fold {fold} ...")

        # Train model
        proj_ae = autoencoder.fit(s_train[set_fold[0]], s_train[set_fold[0]],
                                  epochs=n_epochs,
                                  batch_size=batch_size,
                                  validation_data=(s_train[set_fold[1]], s_train[set_fold[1]]),
                                  callbacks=callbacks,
                                  verbose=1)

        # Increase fold number
        fold = fold + 1

# %% S1.6

# Load model
proj_ae = load_model(main_dir + "Models/" + model_name + "_" + str(n_hls) +
                     "HL_" + data_name + "_" + act_fun_hl + "_" +
                     str(comp_dim) + "nod_lr_" + str(lr) + "_drop" +
                     str(drop_prob) + ".h5")

# Load train and validation loss across folds
loss_log = pd.read_csv((main_dir + "Models/" + model_name + "_" + str(n_hls) +
                     "HL_" + data_name + "_" + act_fun_hl + "_" +
                     str(comp_dim) + "nod_lr_" + str(lr) + "_drop" +
                     str(drop_prob) + ".csv"),
                       sep="\t", na_values="NA", index_col=0)

# Plot training and validation loss at each epoch (from loss log)
train_loss = loss_log["loss"]
val_loss = loss_log["val_loss"]
n_epochs_plot = range(1, len(train_loss) + 1)
plt.plot(n_epochs_plot, train_loss, "y",
         label="Training loss",
         color="cornflowerblue")
plt.plot(n_epochs_plot, val_loss, "r",
         label="Validation loss",
         color="navy")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend()
plt.show()

# Evaluate model performance on the test set (R2 and MAE)
pred_test = proj_ae.predict(s_test)
test_r2 = r2_score(np.transpose(s_test),
                    np.transpose(pred_test))
print("Test Set R2 = ", test_r2)

mae = mean_absolute_error(s_test, pred_test)
print('MAE: %.3f' % mae)
